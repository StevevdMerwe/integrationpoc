package za.co.omsa.integration.poc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.GenericSelector;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.dsl.FileWritingMessageHandlerSpec;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.MessageHandler;

import java.io.File;

/**
 * JavaDSLFileCopyConfig contains various Integration Flows created from various spring integration components.
 * Activate only one flow at a time by un-commenting @Bean annotation from IntegrationFlow beans.
 */
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class JavaDSLFileCopyConfig {


    @Bean
    public IntegrationFlow fileMover(@Value("${input-directory:in}") File in,
                                     @Value("${output-directory:out}") File out) {
        return IntegrationFlows
            .from(Files.inboundAdapter(in).autoCreateDirectory(true).preventDuplicates(true),
                poller -> poller.poller(pm -> pm.fixedRate(1000))
            )
            .filter(onlyJpgs())
            .log(LoggingHandler.Level.INFO)
            .handle(getMessageHandlerSpec(out))
            .log(LoggingHandler.Level.INFO)
            .get();
    }

    private FileWritingMessageHandlerSpec getMessageHandlerSpec(File out) {
        return Files
            .outboundAdapter(out)
            .autoCreateDirectory(true);
    }


    @Bean
    public GenericSelector<File> onlyJpgs() {
        return source -> source.getName()
            .endsWith(".jpg");
    }

}
